name:Simp1e Solarized Light

shadow:002b36
shadow_opacity:0.35

cursor_border:586e75

default_cursor_bg:fdf6e3
hand_bg:fdf6e3

question_mark_bg:268bd2
question_mark_fg:fdf6e3

plus_bg:859900
plus_fg:fdf6e3

link_bg:6c71c4
link_fg:fdf6e3

move_bg:b58900
move_fg:fdf6e3

context_menu_bg:2aa198
context_menu_fg:fdf6e3

forbidden_bg:fdf6e3
forbidden_fg:dc322f

magnifier_bg:fdf6e3
magnifier_fg:586e75

skull_bg:fdf6e3
skull_eye:586e75

spinner_bg:fdf6e3
spinner_fg1:586e75
spinner_fg2:586e75
