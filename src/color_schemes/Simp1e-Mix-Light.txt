name:Simp1e Mix Light

shadow:000000
shadow_opacity:0.35

cursor_border:000000

default_cursor_bg:ffffff
hand_bg:ffffff

question_mark_bg:3daee9
question_mark_fg:ffffff

plus_bg:11d116
plus_fg:ffffff

link_bg:16a085
link_fg:ffffff

move_bg:f67400
move_fg:ffffff

context_menu_bg:3daee9
context_menu_fg:ffffff

forbidden_bg:ffffff
forbidden_fg:ed1515

magnifier_bg:ffffff
magnifier_fg:000000

skull_bg:ed1515
skull_eye:ffffff

spinner_bg:ffffff
spinner_fg1:000000
spinner_fg2:000000
