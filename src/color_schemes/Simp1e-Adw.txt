name:Simp1e Adw

shadow:000000
shadow_opacity:0.35

cursor_border:333333

default_cursor_bg:fafafa
hand_bg:fafafa

question_mark_bg:3584e4
question_mark_fg:ffffff

plus_bg:2ec27e
plus_fg:ffffff

link_bg:c061cb
link_fg:ffffff

move_bg:ffa348
move_fg:ffffff

context_menu_bg:1c71d8
context_menu_fg:ffffff

forbidden_bg:ffffff
forbidden_fg:e01b24

magnifier_bg:fafafa
magnifier_fg:333333

skull_bg:fafafa
skull_eye:333333

spinner_bg:fafafa
spinner_fg1:333333
spinner_fg2:333333
